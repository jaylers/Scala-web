package controllers.connection
import slick.jdbc.PostgresProfile

object PostgresDbProvider{

  val driver = PostgresProfile
  import driver.api._
  val db = Database.forConfig("myPostgresDB")
}