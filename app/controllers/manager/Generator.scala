package controllers.manager
import org.apache.commons.codec.digest.DigestUtils

object Generator {

  def generate(message: String): String = {
    DigestUtils.md5Hex(message)
  }
}
