package controllers.manager

import controllers.components.UsersComponent
import models.{Loginuser, Users}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object UsersManager {
  import controllers.connection.PostgresDbProvider.db

  def register(user: Users): Int ={
    if (isExisting(user.email) < 1){
      Await.result(db.run(UsersComponent.insert(user)), Duration.Inf)
    } else 2
  }

  def loginNow(user : Loginuser): Option[Users] ={
    Await.result(db.run(UsersComponent.getSignIn(user.email.toLowerCase, user.password)), Duration.Inf).headOption
  }

  def getMe(email:String): List[Users] = {
    Await.result(db.run(UsersComponent.getMe(email)), Duration.Inf)
  }

  def getAllUsersWithOutMe(email:String): List[Users] ={
    Await.result(db.run(UsersComponent.getAllWithoutMe(email)), Duration.Inf)
  }

  def isExisting(email: String): Int ={
    Await.result(db.run(UsersComponent.getExisting(email)), Duration.Inf).size
  }
}
