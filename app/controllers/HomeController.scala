package controllers

import java.sql.Timestamp
import java.time
import javax.inject.Inject

import controllers.manager.{Generator, PostManager, UsersManager}
import models._
import play.api.data.Form
import play.api.mvc._

class HomeController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {
  import controllers.formview.PostForm._
  private val postSet = scala.collection.mutable.ArrayBuffer( PrePost("content") )
  private val postUrl = routes.HomeController.home()
  private val readUrl = routes.HomeController.home()

  ////////////////////////////////////////////////////////////////////////////////////////////////////
  def home() = Action { implicit request: MessagesRequest[AnyContent] =>
    request.session.get("email").map( email =>
      request.session.get("name").map { user =>
        val errorLoginFunction = { _: Form[PostData] =>
          BadRequest(views.html.active.post.home(postSet, postForm, postUrl, PostManager.getMyPost(email), UsersManager.getMe(email)))
        }

        val successLoginFunction = { data: PostData =>
          val url = routes.HomeController.home()
          PostManager.postNow(Post(hashId(email) , data.content, Timestamp.valueOf(time.LocalDateTime.now()), email, "5b39c8b553c821e7cddc6da64b5bd2ee.png"))
          Redirect(url)
        }
        val formValidationLoginResult = postForm.bindFromRequest
        formValidationLoginResult.fold(errorLoginFunction, successLoginFunction)
      }.getOrElse {
        Ok(views.html.error.permission_denied()).withNewSession
      }
    ).getOrElse {
      Ok(views.html.error.permission_denied()).withNewSession
    }
  }

  def hashId(email: String): String ={
    Generator(email.concat(Timestamp.valueOf(time.LocalDateTime.now()).toString))
  }

  def getHome() = Action { implicit request:MessagesRequest[AnyContent] =>
    request.session.get("email").map( email =>
      request.session.get("name").map { user =>
        Ok(views.html.active.post.home(postSet, postForm, postUrl, PostManager.getMyPost(email), UsersManager.getMe(email)))
      }.getOrElse {
        Ok(views.html.error.permission_denied()).withNewSession
      }
    ).getOrElse {
      Ok(views.html.error.permission_denied()).withNewSession
    }
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  def news = Action { implicit request: MessagesRequest[AnyContent] =>
    request.session.get("name").map( name =>
      request.session.get("email").map { user =>
        Ok(views.html.active.post.news(user, name, PostManager.getAllPostWithoutMe(user), readUrl))
      }.getOrElse {
        Ok(views.html.error.permission_denied()).withNewSession
      }
    ).getOrElse{
      Ok(views.html.error.permission_denied()).withNewSession
    }
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  def friends = Action { implicit request: MessagesRequest[AnyContent] =>
    request.session.get("email").map { email =>
      request.session.get("name").map { name =>
        Ok(views.html.active.users.friends(email, name, getHead(email), getTail(email)))
      }.getOrElse { Ok(views.html.error.permission_denied()).withNewSession }
    }.getOrElse { Ok(views.html.error.permission_denied()).withNewSession }
  }

  def getHead(email:String): List[Users] ={
    val users = UsersManager.getAllUsersWithOutMe(email)
    users.size%2 match {
      case 0 => { users.take(users.size/2) }
      case 1 => { users.take((users.size/2)+1) }
    }
  }

  def getTail(email:String): List[Users] ={
    val users = UsersManager.getAllUsersWithOutMe(email)
    users.size%2 match {
      case 0 => { users.drop(users.size/2) }
      case 1 => { users.drop((users.size/2)+1) }
    }
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  def profile = Action { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.index.index()).withNewSession
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  def account = Action {implicit  request: MessagesRequest[AnyContent] =>
    request.session.get("email").map( email =>
      request.session.get("name").map { name =>
        Ok(views.html.active.users.account(UsersManager.getMe(email), PostManager.getMyPost(email)))
      }.getOrElse {
        Ok(views.html.error.permission_denied()).withNewSession
      }
    ).getOrElse {
      Ok(views.html.error.permission_denied()).withNewSession
    }
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////
  def logout = Action { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.index.index()).withNewSession
  }
}