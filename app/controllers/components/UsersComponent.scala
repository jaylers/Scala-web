package controllers.components


import java.sql.Timestamp

import controllers.connection.PostgresDbProvider.driver.api._
import models.Users
import slick.dbio.Effect
import slick.sql.{FixedSqlAction, FixedSqlStreamingAction}

import scala.concurrent.ExecutionContext.Implicits.global

object UsersComponent {
  val usersTableQuery = TableQuery[UsersTable]

  def insert(user: Users):  FixedSqlAction[Int, NoStream, Effect.Write] =  {
    usersTableQuery += user
  }

  def delete(mail: String): FixedSqlAction[Int, NoStream, Effect.Write] =  {
    usersTableQuery.filter(x => x.email === mail).delete
  }

  def getAll :  FixedSqlStreamingAction[List[Users], Users, Effect.Read] =  {
    usersTableQuery.to[List].result
  }

  def getSignIn(email: String, password:String) : DBIOAction[List[Users], NoStream, Effect.Read] = {
    usersTableQuery.filter(x => x.email === email && x.password === password).result.map(_.toList)
  }

  def getExisting(email: String) : FixedSqlStreamingAction[List[Users], Users, Effect.Read] = {
    usersTableQuery.filter(x => x.email === email).to[List].result
  }

  def getMe(email: String) : FixedSqlStreamingAction[List[Users], Users, Effect.Read] = {
    usersTableQuery.filter(x => x.email === email).to[List].result
  }

  def getAllWithoutMe(email: String) : FixedSqlStreamingAction[List[Users], Users, Effect.Read] = {
    usersTableQuery.filter(x => x.email =!= email).filter(x => x.privacy =!= true).sortBy(_.name.asc).to[List].result
  }

  class UsersTable(tag: Tag) extends Table[Users] (tag, "users") {
    val uid = column[String]("uid" , O.PrimaryKey)
    val email = column[String]("email")
    val name = column[String]("name")
    val password = column[String]("password")
    val username = column[String]("username")
    val privacy = column[Boolean]("privacy")
    val regdate = column[Timestamp]("regdate")
    val imgurl = column[String]("imgurl")
    def * = (uid, email, name, password, username, privacy, regdate, imgurl) <> (Users.tupled, Users.unapply)
  }
}