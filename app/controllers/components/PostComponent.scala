package controllers.components

import java.sql.Timestamp

import controllers.connection.PostgresDbProvider.driver.api._
import models.Post
import slick.dbio.Effect
import slick.sql.{FixedSqlAction, FixedSqlStreamingAction}

object PostComponent {
  val postTableQuery = TableQuery[PostTable]
  val usersTableQuery = TableQuery[UsersComponent.UsersTable]

  def insert(post:Post): FixedSqlAction[Int, NoStream, Effect.Write] ={ postTableQuery += post }

  def delete(id: Int): FixedSqlAction[Int, NoStream, Effect.Write] = { postTableQuery.filter(x => x.id === id).delete }

  def getAll : FixedSqlStreamingAction[List[Post], Post, Effect.Read] = {
    postTableQuery.to[List].sortBy(_.datetime.desc).result }

  def getAllPostWithoutMe(email: String): FixedSqlStreamingAction[List[(String, Timestamp, String, String, String, String, String, String)], (String, Timestamp, String, String, String, String, String, String), Effect.Read] = {
    val post = for {
      (p, u) <- postTableQuery join usersTableQuery on(_.email === _.email)
    } yield (p.content, p.datetime, p.email, u.username, u.name, u.imgurl, p.post_img_url, p.id)
    post.filter(x => x._3 =!= email).sortBy(_._2 desc).to[List].result
  }

  def sortByDatTime()  : FixedSqlStreamingAction[List[Post], Post, Effect.Read] = {
    postTableQuery.sortBy(x => x.datetime).to[List].result}

  def searchByUsers(email: String) : FixedSqlStreamingAction[List[Post], Post, Effect.Read] = {
    postTableQuery.filter(x => x.email === email).to[List].sortBy(_.datetime.desc).result}

  class PostTable(tag: Tag) extends Table[Post] (tag, "post") {
    val id = column[String]("id" , O.PrimaryKey)
    val content = column[String]("content")
    val datetime = column[Timestamp]("datetime")
    val email = column[String]("email")
    val post_img_url = column[String]("post_img_url")
    def * = (id, content, datetime, email, post_img_url) <> (Post.tupled, Post.unapply)
  }
}