package controllers

import javax.inject.Inject

import play.api.mvc._

class StartController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {

  def index = Action { implicit request: MessagesRequest[AnyContent] =>
    request.session.get("email").map { mEmail =>
      request.session.get("name").map { mName =>
        if (mEmail.length > 3){ val url = routes.HomeController.home
          Redirect(url).withSession("email" -> mEmail, "name" -> mName)}
        else {Ok(views.html.index.index()).withNewSession}
      }.getOrElse { Ok(views.html.index.index()).withNewSession }
    }.getOrElse { Ok(views.html.index.index()).withNewSession }
  }

  def about_me = Action { Ok(views.html.index.aboutme()) }
}